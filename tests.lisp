(defpackage #:clamber/tests
  (:use #:cl)
  (:export #:run-tests))
(in-package clamber/tests)

(5am:in-suite* clamber)

(defparameter *test-db-file*
  (uiop:xdg-data-home "clamber" "test-books.db"))

(defun run-tests ()
  (let ((clamber::*database-file* *test-db-file*))
    (5am:run! 'clamber)))

(5am:test insert
  (5am:is
   (clamber:insert-book
    :foo1
    :shelf "nonfiction"
    :tags '("nonfiction" "drama")))

  (5am:is
   (clamber:insert-book
    :foo2
    :shelf "psychology"
    :tags '("psychology" "drama" "fiction")))

  (5am:is
   (clamber:insert-book
    :foo3
    :shelf "psychology"
    :tags '("drama" "fiction")))

  (5am:signals simple-error
    (clamber:insert-book
     :foo3
     :shelf "erroneous"
     :tags "erroneous")))

(5am:test find-books
  (5am:is (= 3 (length (clamber:find-books :tags "drama"))))
  (5am:is (= 2 (length (clamber:find-books :shelf "psychology"))))
  (5am:is (= 2 (length (clamber:find-books :tags "fiction"))))
  (5am:is (= 2 (length (clamber:find-books :tags '("drama" "fiction")))))
  (5am:is (= 1 (length (clamber:find-books :tags '("drama" "nonfiction")))))
  (5am:is (= 0 (length (clamber:find-books :tags '("psychology" "nonfiction")))))
  (5am:is (= 3 (length (clamber:find-books))))
  (5am:is (clamber:find-books :book-designator :foo3))
  (5am:is (null (clamber:find-books :book-designator :foo4))))

(5am:test delete-book
  (5am:is (clamber:delete-book :foo1))
  (5am:is (null (clamber:delete-book :foo1)))
  (5am:is (null (clamber:delete-book :foo1)))
  (5am:is (= 2 (length (clamber:find-books))))
  (5am:is (clamber:delete-book
           (clamber:book-designator (car (clamber:find-books)))))
  (5am:is (= 1 (length (clamber:find-books))))
  (5am:is (clamber:delete-book
           (clamber:book-designator (car (clamber:find-books)))))
  (5am:is (= 0 (length (clamber:find-books)))))
