;;;; clamber.asd

(asdf:defsystem #:clamber
  :description "Book collection managament."
  :author "Daniel Kochmański <daniel@turtleware.eu>"
  :license "Public Domain"
  :depends-on (#:alexandria
               #:split-sequence
               #:uiop
               #:cl-store
               #:net.didierverna.clon)
  :serial t
  :components ((:file "clamber")
               (:file "cli"))
  :in-order-to ((asdf:test-op
                 (asdf:test-op #:clamber/tests))))

(asdf:defsystem #:clamber/tests
  :depends-on (#:clamber #:fiveam)
  :components ((:file "tests"))
  :perform (asdf:test-op (o s)
             (uiop:symbol-call :clamber/tests :run-tests)))
