;;;; clamber.lisp
(defpackage #:clamber
  (:use #:cl)
  (:export #:insert-book
           #:delete-book
           #:find-books
           #:book-designator))

(in-package #:clamber)


;;; Clamber book managament protocol

;;; Requirements explicitly list that books has to be organized by
;;; shelfs and tags. Book designator is used to identify books (it has
;;; to be unique). Protocol doesn't mandate designator type. It may be
;;; an unique name, pathname, URL or any arbitrary object. Other args
;;; (in form of keys) are meant to contain metainformation.
(defgeneric insert-book (book-designator &rest args
                         &key shelf tags &allow-other-keys)
  (:documentation "Creates a book entity associated to a given ~
   `shelf' and `tags'."))

;;; We need to bo able to remove book. We need only the designator for
;;; that.
(defgeneric delete-book (book-designator)
  (:documentation "Removes a book entity from the system."))

;;; We may search for books according to various
;;; criteria. `book-designator' is definite. It is possible to extend
;;; the find functionality to support other criteria. Book must match
;;; *all* supplied criteria.
(defgeneric find-books (&rest args
                        &key book-designator shelf tags &allow-other-keys)
  (:documentation "Returns a `sequence' of books matching the ~
  requirements."))

;;; We access books by their designators, but `find-books' returns a
;;; list of opaque objects. This function is needed for coercion from
;;; these objects to the designators. Sample usage:
;;; 
;;; (map 'list #'book-designator (find-books :shelf "criminal"))
(defgeneric book-designator (book)
  (:documentation "Extract book designator from opaque `book' object."))


;;; Implementation

;;; At start we are going to work on in-memory database.
(defparameter *all-books* (make-hash-table) "All defined books.")

;;; Note, that when we define `:reader' for the slot `designator' we
;;; actually implement part of the protocol.
(defclass book ()
  ((designator :type symbol   :initarg :des :reader book-designator)
   (shelf      :type string   :initarg :shl :reader book-shelf)
   (tags       :type sequence :initarg :tgs :reader book-tags)
   (meta :initarg :meta :accessor book-information)))

(defmethod print-object ((object book) stream)
  (if (not *print-escape*)
      (format stream "~10s [~10s] ~s -- ~s"
              (book-designator object)
              (book-shelf object)
              (book-tags object)
              (book-information object))
      (call-next-method)))

;;; `title' and `author' are enlisted for completion.
(defmethod insert-book ((designator symbol) &rest args
                        &key shelf tags title author &allow-other-keys
                        &aux (tags (alexandria:ensure-list tags)))
  (declare (ignore title author readedp)
           (type (shelf string)))
  (multiple-value-bind (book found?) (gethash designator *all-books*)
    (declare (ignore book))
    (if found?
        (error "Book with designator ~s already present." designator)
        (setf (gethash designator *all-books*)
              (make-instance 'book
                             :des designator
                             :shl shelf
                             :tgs (coerce tags 'list)
                             :meta args)))))

;;; Trivial
(defmethod delete-book ((designator symbol))
  (remhash designator *all-books*))

;;; We use `while-collecting' macro (`collect' equivalent from
;;; cmu-utils) to simplify the code.
(defmethod find-books (&rest args
                       &key book-designator shelf tags
                       &allow-other-keys
                       &aux (tags (alexandria:ensure-list tags)))
  (declare (ignore args))
  (uiop:while-collecting (match)
    (labels ((match-book (book)
               (and (or (null shelf)
                        (equalp shelf (book-shelf book)))
                    (or (null tags)
                        (subsetp tags (book-tags book) :test #'equalp))
                    (match book))))
      (if book-designator
          (alexandria:when-let ((book (gethash book-designator *all-books*)))
            (match-book book))
          (alexandria:maphash-values (lambda (val)
                                       (match-book val))
                                     *all-books*)))))


;;; Persistence
(defparameter *database-file* (uiop:xdg-data-home "clamber" "books.db"))

(defun restore-db ()
  "Restore a database from the file."
  (when (probe-file *database-file*)
    (setf *all-books* (cl-store:restore *database-file*))))

(defun store-db ()
  "Store a database in the file."
  (ensure-directories-exist *database-file*)
  (cl-store:store *all-books* *database-file*))

(defmethod insert-book :around ((designator symbol)
                                &rest args &key &allow-other-keys)
  (declare (ignore designator args))
  (prog2 (restore-db)
      (call-next-method)
    (store-db)))

(defmethod delete-book :around ((designator symbol))
  (declare (ignore designator))
  (prog2 (restore-db)
      (call-next-method)
    (store-db)))

(defmethod find-books :around (&rest args &key &allow-other-keys)
  (declare (ignore args))
  (restore-db)
  (call-next-method))
